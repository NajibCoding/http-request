# HTTP Request
Request data ke API lebih mudah dengan library HTTP Request untuk Codeigniter 3.

## Instalasi

Pindahkan file Http.php kedalam folder library codeigniter 3, pada path berikut: application\libraries\Http.php

## Penggunaan
Untuk menggunakannya kita panggil terlebih dahulu librarynya, menggunakan perintah:

```php
$this->load->library('http');
```
atau

Kita bisa membuat library ini otomatis terload, dengan menambahkan settingan pada autoload.php, kalian bisa menambahkannya pada folder berikut: 

**application\config\autoload.php**

Tambahkan library HTTP Request dengan code berikut:

```php
$autoload['libraries'] = array('http');
```

Bila sudah dipanggil, librarynya. Saatnya kita gunakan dengan perintah seperti berikut:

### GET

Tanpa parameter:
```php
$this->http->url("https://api.com")
           ->call()
           ->toJson();
```
Dengan parameter:
```php
$this->http->url("https://api.com")
           ->data("username", "foo")
           ->data("password", "bar")
           ->data(array("ip" => "127.0.0.1", "name" => "roo"))
           ->call()
           ->toJson();
```

### POST
Tanpa data:
```php
$this->http->url("https://api.com")
           ->method("POST")
           ->call()
           ->toJson();
```
Dengan data:

```php
$this->http->url("https://api.com")
           ->method("POST")
           ->data("username", "foo")
           ->data("password", "bar")
           ->data(array("ip" => "127.0.0.1", "name" => "roo"))
           ->call()
           ->toJson();
```

### HEAD
Tanpa data:
```php
$this->http->url("https://api.com")
           ->method("HEAD")
           ->call()
           ->toJson();
```
Dengan data:
```php
$this->http->url("https://api.com")
           ->method("HEAD")
           ->data("username", "foo")
           ->data("password", "bar")
           ->data(array("ip" => "127.0.0.1", "name" => "roo"))
           ->call()
           ->toJson();
```

### PUT
Tanpa data:
```php
$this->http->url("https://api.com")
           ->method("PUT")
           ->call()
           ->toJson();
```
Dengan data:
```php
$this->http->url("https://api.com")
           ->method("PUT")
           ->data("username", "foo")
           ->data("password", "bar")
           ->data(array("ip" => "127.0.0.1", "name" => "roo"))
           ->call()
           ->toJson();
```

## Konfigurasi

### URL (Wajib)
```
url(string $url)
```

### User Agent (Optional)
```
ua(string $ua)
```

### Method (Optional - Bila ingin request GET saja)
```
method(string $method = "GET")
```
Kalian bisa menggunakan beberapa method berikut:
- GET
- POST
- HEAD
- PUT
- PATCH
- DELETE
- TRACE

### Data (Optional)
```
data(array/string $data, $value = null)
```
Contoh penggunaan function data():
#### Banyak Data
Kalian dapat mengirim banyak data sekaligus dengan menggunakan array, pada contoh berikut:
```php
$data = array(
    'nama' => "Muhamad Najib",
    'usia' => 22,
    'pekerjaan' => "Programmer"
);
$this->http->data($data);
```
#### Data Satuan
```php
$this->http->data('nama', "Muhamad Najib")
           ->data('usia', 22)
           ->data('pekerjaan', "Programmer");
```

### SSL (Optional)
```
ssl(bool $ssl = true)
```

Function ini berguna untuk memverifikasi sertifikat ssl sesuai dengan host dan memverifikasi sertifikat ssl yang sah.


### Call (Wajib)
```
call();
```
Function ini digunakan untuk request http ke url yang dituju. Untuk menampilkan output, gunakan perintah __toJson()__ atau __toString()__, panggil salah satu.

### ToJson (Wajib Dipanggil salah satu - Lihat pada function Call())
```
toJson();
```
Function toJson berguna untuk menampilkan data hasil convert dari string json(__json_encode()__) ke data json.

### ToString (Wajib Dipanggil salah satu - Lihat pada function Call())
```
toString();
```
Function toString berguna untuk menampilkan data string .

## Kontribusi
Silahkan di clone dan di pull, bila ada error/penambahan jangan sungkan untuk membuat issue.
