<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Http
{
    private $url;
    private $method;
    private $data = array();
    private $ua;
    private $ssl;
    private $toStreams;
    private $verbose;
    private $header = array();
    private $output;

    /**
     * Class constructor
     *
     * @param	array	$params
     * @return	void
     */
    public function __construct()
    {
    }
    /**
     * Class constructor
     *
     * @param	string	$ua
     * @return	void
     */
    public function url(string $url)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->url)) throw new Exception("Function ini sudah digunakan", 403);
        $this->url = $url;
        return $this;
    }

    /**
     * Class constructor
     *
     * @param	string	$ua
     * @return	void
     */
    public function ua(string $ua)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->ua)) throw new Exception("Function ini sudah digunakan", 403);
        $this->ua = $ua;
        return $this;
    }

    /**
     * Class constructor
     *
     * @param	string	$method
     * @return	void
     */
    public function method(string $method = "GET")
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->method)) throw new Exception("Function ini sudah digunakan", 403);
        if (!in_array($method, ["GET", "POST", "DELETE", "PUT", "PATCH", "TRACE", "HEAD"])) throw new Exception("Method tidak ada!", 405);
        $this->method = $method;
        return $this;
    }

    /**
     * Class constructor
     *
     * @param	string/array	$data
     * @param	string/int	$data
     * @return	void
     */
    public function data($data, $value = null)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (is_array($data)) {
            foreach ($data as $row => $value) {
                $this->data[$row] = $value;
            }
        } else {
            $this->data[$data] = $value;
        }
        return $this;
    }

    /**
     * Class constructor
     *
     * @return	void
     */

    public function call()
    {        
        //Check curl version
        $curl = curl_version();
        $curl = $curl['version_number'];
        $verifHost = (empty($this->ssl) || $this->ssl) ? 2 : 0;
        if($curl < 465921) $verifHost = (empty($this->ssl) || $this->ssl) ? 1 : 0; //Kalo curlnya di bawah versi < 7.28.1
        if (empty($this->url)) throw new Exception("URL belum digunakan", 403);

        if (isset($this->header)) {
            foreach ($this->header as $row) {
                header($row);
            }
        }
        $ch = curl_init();
        $url = $this->url;
        
        if (count($this->data) > 0 && (empty($this->thod) || $this->method == 'GET')) $url .= '?' . http_build_query($this->data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, (bool)$this->verbose);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, empty($this->toStreams) ? true : (bool)$this->toStreams);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $verifHost);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, empty($this->ssl) ? true : (bool)$this->ssl);
        if (empty($this->method) || $this->method == 'GET') {
            $this->method = 'GET';
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->method);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data));
        }
        if ($this->ua) {
            curl_setopt($ch, CURLOPT_USERAGENT, $this->ua);
        }
        $this->output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);
        return $this;
    }

    public function toJson()
    {
        if (empty($this->output)) throw new Exception("Panggil APInya terlebih dahulu");
        $output = $this->output;
        $this->reset();
        return json_decode($output, true);
    }

    public function toString()
    {
        if (empty($this->output)) throw new Exception("Panggil APInya terlebih dahulu");
        $this->reset();
        return $this->output;
    }

    /**
     * Class constructor
     *
     * @return	void
     */
    public function toStreamOutput(bool $output = true)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->toStreams)) throw new Exception("Function ini sudah digunakan", 403);
        $this->toStreams = $output;
        return $this;
    }

    /**
     * Class constructor
     *
     * @param	string/array	$header
     * @return	void
     */
    public function header($header, $value = "", $escaped = true)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if ((bool) $escaped === true) {
            if (is_array($value)) throw new Exception("Tidak bisa escaped header dengan nilai array", 400);
            $this->header[] = $header;
        }

        if (is_array($header)) {
            foreach ($header as $row => $value) {
                $this->header[] = "{$row}: {$value};";
            }
        } else {
            $this->header[] = "{$header}: {$value};";
        }
        return $this;
    }

    /**
     * Class constructor
     *
     * @param bool $ssl
     * @return	void
     */
    public function ssl(bool $ssl = true)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->ssl)) throw new Exception("Function ini sudah digunakan", 403);
        $this->ssl = $ssl;
        return $this;
    }
    /**
     * Class constructor
     *
     * @param bool $verbose
     * @return	void
     */
    public function verbose(bool $verbose = false)
    {
        if (!empty($this->output)) throw new Exception("API sudah terpanggil, Taruh function ini di depan function call().");
        if (!empty($this->verbose)) throw new Exception("Function ini sudah digunakan", 403);
        $this->ssl = $verbose;
        return $this;
    }

    public function reset()
    {
        $this->url = null;
        $this->method = null;
        $this->data = array();
        $this->ua = null;
        $this->ssl = null;
        $this->toStreams = null;
        $this->verbose = null;
        $this->header = array();
        $this->output = null;
        return true;
    }
}
